/*
 * Copyright (c) 2018. OsirptPm
 * 이 저작물에는 크리에이티브 커먼즈 저작자표시-비영리-동일조건변경허락 4.0 국제 라이선스가 적용 되어 있습니다. 이 라이선스의 설명을 보고 싶으시면 http://creativecommons.org/licenses/by-nc-sa/4.0/ 을(를) 참조하세요.
 */

const request = require("request");
const { StatusError } = require("./CustomError");
const config = require("../config");

let apiOptions = config.api;


function requestPromise(postData, path, method = "POST") {
    let requestOptions = {
        url: path,
        method: method,
        json: postData || {},
        strictSSL: apiOptions.strictSSL
    };

    return new Promise((resolve, reject) => {
        request(requestOptions, (err, response, result) => {
            if (err) return reject(err);
            if (response) {
                if (response.statusCode === 200 && result.success) {
                    resolve(result);
                } else {
                    reject(new StatusError(result.message, response.statusCode));
                }
            } else reject(new StatusError("응답 없음", 504));
        });
    });
}

function requestPromiseExternalAPI(postData, path, method = "GET") {
    let requestOptions = {
        url: path,
        method: method,
        json: postData || {},
        strictSSL: apiOptions.strictSSL
    };

    return new Promise((resolve, reject) => {
        request(requestOptions, (err, response, result) => {
            if (err) return reject(err);
            if (response) {
                if (response.statusCode === 200) {
                    resolve(result);
                } else {
                    reject(new StatusError(result.message, response.statusCode));
                }
            } else reject(new StatusError("응답 없음", 504));
        });
    });
}

module.exports.requestPromise = requestPromise;

module.exports.requestPromiseExternalAPI  = requestPromiseExternalAPI;