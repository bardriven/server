/*
 * Copyright (c) 2018. OsirptPm
 * 이 저작물에는 크리에이티브 커먼즈 저작자표시-비영리-동일조건변경허락 4.0 국제 라이선스가 적용 되어 있습니다. 이 라이선스의 설명을 보고 싶으시면 http://creativecommons.org/licenses/by-nc-sa/4.0/ 을(를) 참조하세요.
 */

const { requestPromise } = require("../helpers/CustomRequest");
const { StatusError } = require("../helpers/CustomError");
const errorHandler = require("../helpers/errorHandler");
const config = require("../config");

let apiOptions = config.api;

/*
    input: userToken
    output: user
 */
module.exports = (req, res, next) => {
    let usertoken = req.query.usertoken;
    let userPath = "/users/auth/me" + "/?usertoken=" + usertoken + "&apikey=" + apiOptions.APIKey;

    if (!usertoken) return next(new StatusError("잘못된 접근", 412));


    const handler = (result) => {
        req.user = result.data;
        req.user.token = req.query.usertoken;
        return Promise.resolve();
    };

    const getThings = () => {
        let tokenPath = "/things/auth/token" + "/?apikey=" + apiOptions.APIKey;
        let user = req.user;
        // 비동기 순차 코드
        let p = Promise.resolve();
        for (let i = 0; i < user.things.length; i++) {
            p = p.then(_ => {
                return new Promise((resolve, reject) => {
                    let postData = {
                        thing: user.things[i]
                    };
                    requestPromise(postData, apiOptions.api_server + tokenPath)
                        .then(result => {
                            user.things[i].token = result.data;
                            resolve(user);
                        })
                        .catch(reject);
                }).catch(err => {
                    i = user.things.length;
                    throw err;
                });
            });
        }
        return p;
    };

    const respond = () => {
        next();
    };

    const onError = (error) => {
        if (error.message === "jwt expired") {
            res.redirect("/user/signout");
        } else
            errorHandler(error, req, res, next);// 회원가입 시 실패시 오류 페이지로 넘어가버림, 다른 곳에서 사용하는 경우가 없으면 수정필요
    };

    requestPromise(null, apiOptions.api_server + userPath, "GET")
        .then(handler)
        .then(getThings)
        .then(respond)
        .catch(onError);
};