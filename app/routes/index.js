/*
 * Copyright (c) 2018. OsirptPm
 * 이 저작물에는 크리에이티브 커먼즈 저작자표시-비영리-동일조건변경허락 4.0 국제 라이선스가 적용 되어 있습니다. 이 라이선스의 설명을 보고 싶으시면 http://creativecommons.org/licenses/by-nc-sa/4.0/ 을(를) 참조하세요.
 */

const express = require("express");
const router = express.Router();
const controller = require("../controllers/index");
const auth = require("../middlewares/auth");

/* GET home page. */
router.get("/", controller.home);
router.get("/info", controller.info);
router.get("/control_board", auth, controller.control_board);
router.get("/dashboard", auth, controller.dashboard);
router.get("/settings", auth, controller.settings);

// ajax request
router.get("/ajax/getNearbyMsrstnList", controller.getNearbyMsrstnList);
router.get("/ajax/ArpltnInforInqireSvc", controller.ArpltnInforInqireSvc);

module.exports = router;
