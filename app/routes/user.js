/*
 * Copyright (c) 2018. OsirptPm
 * 이 저작물에는 크리에이티브 커먼즈 저작자표시-비영리-동일조건변경허락 4.0 국제 라이선스가 적용 되어 있습니다. 이 라이선스의 설명을 보고 싶으시면 http://creativecommons.org/licenses/by-nc-sa/4.0/ 을(를) 참조하세요.
 */

const express = require("express");
const router = express.Router();
const ctrlUser = require("../controllers/user");
const auth = require("../middlewares/auth");


router.post("/signin", ctrlUser.signin);
router.post("/signup", ctrlUser.signup);
router.post("/password", auth, ctrlUser.password);
router.post("/withdraw", auth, ctrlUser.withdraw);
router.post("/thingAdd", auth, ctrlUser.thingAdd);
router.post("/thingRemove", auth, ctrlUser.thingRemove);
router.get("/signout", ctrlUser.signout);

module.exports = router;
