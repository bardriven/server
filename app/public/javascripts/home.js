/*
 * Copyright (c) 2018. OsirptPm
 * 이 저작물에는 크리에이티브 커먼즈 저작자표시-비영리-동일조건변경허락 4.0 국제 라이선스가 적용 되어 있습니다. 이 라이선스의 설명을 보고 싶으시면 http://creativecommons.org/licenses/by-nc-sa/4.0/ 을(를) 참조하세요.
 */

export default function (args) {
    let { oHashController } = args;
    const onHandler = () => {
        $("#wrap")[0].classList.add("view_signup");
    };
    const offHandler = () => {
        $("#wrap")[0].classList.remove("view_signup");
    };
    oHashController.addHashEvent("view_signup", onHandler, offHandler);

    $("#signupBtn").on("click", () => {
        oHashController.add("view_signup");
    });
    $("#signinBtn").on("click", () => {
        oHashController.remove("view_signup");
    });
    $(() => {$("input[type='email']")[0].focus();});
    // // 이미 있는 경우에 이벤트를 늦게 추가해서 실행 안되는 걸 방지
    // oHashController.toggle("view_signup");
    // oHashController.toggle("view_signup");
};