/*
 * Copyright (c) 2018. OsirptPm
 * 이 저작물에는 크리에이티브 커먼즈 저작자표시-비영리-동일조건변경허락 4.0 국제 라이선스가 적용 되어 있습니다. 이 라이선스의 설명을 보고 싶으시면 http://creativecommons.org/licenses/by-nc-sa/4.0/ 을(를) 참조하세요.
 */

import DrawCircleGraph from "./helpers/DrawCircleGraph.js"
import connectSocket from "./helpers/connectSocket.js"
import geolocation from "./helpers/geolocation.js"
import {calcDiscomfortIndex, DIConverter, pm2_5Converter, pm10Converter, attachPowerClickHandler} from "./helpers/globalHelpers.js"


function initDrawCircleGraph(name) {
    let thCircleCanvas = $("#" + name + " .THCircleCanvas")[0];
    let pm2_5CircleCanvas = $("#" + name + " .pm2_5CircleCanvas")[0];
    let pm10CircleCanvas = $("#" + name + " .pm10CircleCanvas")[0];
    let drawCircleGraph_TH = new DrawCircleGraph(thCircleCanvas, 90 , 50);
    let drawCircleGraph_pm2_5 = new DrawCircleGraph(pm2_5CircleCanvas, 75 , 0);
    let drawCircleGraph_pm10 = new DrawCircleGraph(pm10CircleCanvas, 150 , 0);
    drawCircleGraph_TH.decoration = { lineColor: "#007bff", textPrimaryColor: "rgba(248, 249, 250, 1)", textSecondaryColor: "rgba(248, 249, 250, .8)", textSecondarySize: 15 };
    drawCircleGraph_pm2_5.decoration = { lineColor: "#28a745", textPrimaryColor: "rgba(248, 249, 250, 1)", textSecondaryColor: "rgba(248, 249, 250, .8)", textSecondarySize: 15 };
    drawCircleGraph_pm10.decoration = { lineColor: "#dc3545", textPrimaryColor: "rgba(248, 249, 250, 1)", textSecondaryColor: "rgba(248, 249, 250, .8)", textSecondarySize: 15 };
    return {drawCircleGraph_TH, drawCircleGraph_pm2_5, drawCircleGraph_pm10};
}
/**
 *
 * @param {DrawCircleGraph} drawCircleGraph
 * @param {{value: number, step: number, txt: string, title: string}} value
 */
function drawShortcut(drawCircleGraph, value) {
    let lineColor;
    switch (value.step) {
        case 3: lineColor = "#dc3545"; break;
        case 2: lineColor = "#ffc107"; break;
        case 1: lineColor = "#28a745"; break;
        default: lineColor = "#007bff";
    }
    drawCircleGraph.decoration = {
        textPrimary: value.value.toFixed(1),
        textSecondary: value.title,
        lineColor: lineColor,
        // textPrimaryColor: "rgba(248, 249, 250, 1)",
        // textSecondaryColor: "rgba(248, 249, 250, .8)",
        // textSecondarySize: 15
    };
    drawCircleGraph.draw(value.value);
}


function powerStateUpdate(name, power) {
    let thingSection = $("#"+name);
    let powerStatus = thingSection.find(".powerStatus")[0];
    powerStatus.textContent = (power) ? "켜짐" : "꺼짐";
    let powerStatusIcon = thingSection.find(".powerStatusIcon")[0];
    (power) ? powerStatusIcon.classList.remove("powerOff") : powerStatusIcon.classList.add("powerOff");
}

export default function (args) {
    let { oHashController } = args;


    geolocation();

    /**
     *
     * @var  {{uType: string, provider: string, things: [{_id: string, name: string, __v: number, token: string}], _id: string, email: string, __v: number}} user
     */
    user.thingTokens = [];
    user.things.forEach(thing => {
        user.thingTokens.push(thing.token);
        thing["drawCircleGraphs"] = initDrawCircleGraph(thing.name);
    });
    console.log(user);


    let allAverage = {
        TH: {},
        pm2_5: {},
        pm10: {},
        getTH: function () {
            let {sum, size} = this._sum(this.TH);
            return sum / size;
        },
        getPm2_5: function () {
            let {sum, size} = this._sum(this.pm2_5);
            return sum / size;
        },
        getPm10: function () {
            let {sum, size} = this._sum(this.pm10);
            return sum / size;
        },
        _sum: function (obj) {
            let sum = 0; let size = 0;
            for(let el in obj) {
                if(obj.hasOwnProperty(el)) {
                    sum += parseFloat(obj[el]);
                    size++;
                }
            }
            return {sum, size};
        }
    };

    let thCircleCanvas = $("#inside .THCircleCanvas")[0];
    let pm2_5CircleCanvas = $("#inside .pm2_5CircleCanvas")[0];
    let pm10CircleCanvas = $("#inside .pm10CircleCanvas")[0];
    let drawCircleGraph_TH = new DrawCircleGraph(thCircleCanvas, 90 , 50);
    let drawCircleGraph_pm2_5 = new DrawCircleGraph(pm2_5CircleCanvas, 75 , 0);
    let drawCircleGraph_pm10 = new DrawCircleGraph(pm10CircleCanvas, 150 , 0);
    drawCircleGraph_TH.decoration = { lineColor: "#007bff", textPrimaryColor: "rgba(248, 249, 250, 1)", textSecondaryColor: "rgba(248, 249, 250, .8)" };
    drawCircleGraph_pm2_5.decoration = { lineColor: "#28a745", textPrimaryColor: "rgba(248, 249, 250, 1)", textSecondaryColor: "rgba(248, 249, 250, .8)" };
    drawCircleGraph_pm10.decoration = { lineColor: "#dc3545", textPrimaryColor: "rgba(248, 249, 250, 1)", textSecondaryColor: "rgba(248, 249, 250, .8)" };

    let socket = connectSocket(user);

    socket.on("connect", () => {
        user.things.forEach(thing => {
            attachPowerClickHandler(thing.name, socket);

            let json = {
                name: thing.name,
                line: 50
            };
            socket.emit("normalizedDTH", json);
        });
    });

    socket.on("power", (data) => {
        console.error(data);
    });

    socket.on("realTimeDTH", (result) => {
        let name = result.name;
        let reported = result.data.state.reported;
        let power = reported.power;

        let discomfort = calcDiscomfortIndex(reported.temperature, reported.humidity);

        let thing = user.things.find(thing => thing.name === result.name);

        allAverage.TH[name] = discomfort;
        allAverage.pm2_5[name] = reported.pm2_5;
        allAverage.pm10[name] = reported.pm10;

        drawShortcut(thing.drawCircleGraphs.drawCircleGraph_TH, DIConverter(discomfort));
        drawShortcut(thing.drawCircleGraphs.drawCircleGraph_pm2_5, pm2_5Converter(reported.pm2_5));
        drawShortcut(thing.drawCircleGraphs.drawCircleGraph_pm10, pm10Converter(reported.pm10));

        powerStateUpdate(thing.name, reported.power);

        let averageTH = allAverage.getTH();
        let averagePm2_5 = allAverage.getPm2_5();
        let averagePm10 = allAverage.getPm10();
        drawShortcut(drawCircleGraph_TH, DIConverter(averageTH));
        drawShortcut(drawCircleGraph_pm2_5, pm2_5Converter(averagePm2_5));
        drawShortcut(drawCircleGraph_pm10, pm10Converter(averagePm10));
    });

    socket.on("normalizedDTH", (result) => {
        let thing = user.things.find(thing => thing.name === result.name);
    });

    window.onresize = redraw;
    window.onload = redraw;

    function redraw() {
        let ja_canvases = document.getElementsByTagName("canvas");
        for (let i = 0; i < ja_canvases.length; i++) {
            let width = ja_canvases[i].parentElement.offsetWidth;
            ja_canvases[i].width = width;
            ja_canvases[i].height = width;
        }
        drawCircleGraph_TH.draw();
        drawCircleGraph_pm2_5.draw();
        drawCircleGraph_pm10.draw();
        user.things.forEach(thing => {
            thing.drawCircleGraphs.drawCircleGraph_TH.draw();
            thing.drawCircleGraphs.drawCircleGraph_pm2_5.draw();
            thing.drawCircleGraphs.drawCircleGraph_pm10.draw();
        });
    }
}