/*
 * Copyright (c) 2018. OsirptPm
 * 이 저작물에는 크리에이티브 커먼즈 저작자표시-비영리-동일조건변경허락 4.0 국제 라이선스가 적용 되어 있습니다. 이 라이선스의 설명을 보고 싶으시면 http://creativecommons.org/licenses/by-nc-sa/4.0/ 을(를) 참조하세요.
 */

import connectSocket from "./helpers/connectSocket.js";
import {get1DArrayFrom2DObj, getDTHArr, normalize, attachPowerClickHandler} from "./helpers/globalHelpers.js";
import {Labels, OGraph, OMultiGraph, Queue} from "./libs/OGraph/OMultiGraph.js";

/**
 *
 * @param result
 * @param {OMultiGraph} oMultiGraph
 * @param {string} type
 */
function canvasHandler(result, oMultiGraph, type) {
    let shadows = undefined;
    let predictions = undefined;

    if (type === "normalizedDTH") {
        shadows = result.data;
        let DTHArr = getDTHArr(shadows);

        for (let graphsKey in oMultiGraph.graphs) {
            if (oMultiGraph.graphs.hasOwnProperty(graphsKey) && !/Predict/.test(graphsKey)) {
                let mGraph = oMultiGraph.graphs[graphsKey];
                let type = /[a-z]+_[0-9]+_(\w+)/.exec(graphsKey)[1];
                let qData = DTHArr[type + "Arr"];
                mGraph.graph.queue.add(qData);
            }
        }
    }
    else if (type === "predictDTH") {
        predictions = result.data.predictions[0];
        let pm2_5Predict = [];
        let pm10Predict = [];
        predictions.forEach(arr => {
            pm2_5Predict.push(arr[0]);
            pm10Predict.push(arr[1]);
        });
        let predict = {
            pm2_5Predict,
            pm10Predict
        };
        for (let graphsKey in oMultiGraph.graphs) {
            if (oMultiGraph.graphs.hasOwnProperty(graphsKey) && /Predict/.test(graphsKey)) {
                let mGraph = oMultiGraph.graphs[graphsKey];
                let type = /[a-z]+_[0-9]+_(\w+)/.exec(graphsKey)[1];
                mGraph.graph.queue.add(predict[type]);
            }
        }
    }

    normalizeLabels(oMultiGraph);
    draw(oMultiGraph);
}

function normalizeLabels(oMultiGraph) {
    if (!oMultiGraph.displayLabels) {
        oMultiGraph.displayLabels = new Labels();
    }
    let graphsArr = [];
    for (let graphsKey in oMultiGraph.graphs) {
        if (oMultiGraph.graphs.hasOwnProperty(graphsKey)) {
            graphsArr.push(oMultiGraph.graphs[graphsKey]);
        }
    }

    let visibleGraphs = graphsArr.filter(m => m.graph.visible);
    if (visibleGraphs.length === 0) return;
    // if () {
    //
    // }
    let max = -9999;
    let min = 9999;

    visibleGraphs.forEach(m => {
        if (m.graph.name.includes("temperature") || m.graph.name.includes("humidity")) {
            m.graph.visibleLabel = true;
            m.graph.dynamicNormalize = true;
        } else {
            max = Math.max(max, ...m.graph.queue);
            min = Math.min(min, ...m.graph.queue);
            m.graph.labels = oMultiGraph.displayLabels;
        }
    });
    if (visibleGraphs[visibleGraphs.length - 1].graph.name.includes("pm")) {
        visibleGraphs[visibleGraphs.length - 1].graph.visibleLabel = true;

        let canvas = document.getElementsByTagName("canvas")[0];
        let columnY = canvas.height / 70 - 1;
        let columnX = canvas.width / 200;
        columnY = columnY < 2 ? 2 : columnY;
        columnX = columnX < 2 ? 2 : columnX;


        let labels = oMultiGraph.displayLabels;
        labels.setX(-16, -0.3, columnX, "분");
        labels.setY(min - .5, max + .5, columnY, "㎍/㎥");
    }
}

function draw(oMultiGraph) {
    oMultiGraph.graphNames.forEach(name => {
        oMultiGraph.graphs[name].graph.draw();
    });
}
function powerStateUpdate(name, power) {
    let thingSection = $("#"+name);
    let powerStatus = thingSection.find(".powerStatus")[0];
    powerStatus.textContent = (power) ? "켜짐" : "꺼짐";
    let powerStatusIcon = thingSection.find(".powerStatusIcon")[0];
    (power) ? powerStatusIcon.classList.remove("powerOff") : powerStatusIcon.classList.add("powerOff");
}
function realTimeDTHUpdate(name, reported, speechBubbleMeter, oMultiGraph) {
    let {temperature, humidity, pm2_5, pm10} = speechBubbleMeter;
    temperature.textContent = reported.temperature.toFixed(1);
    humidity.textContent = reported.humidity.toFixed(1);
    pm2_5.textContent = reported.pm2_5.toFixed(1);
    pm10.textContent = reported.pm10.toFixed(1);
    moveBubble(name, temperature, oMultiGraph);
    moveBubble(name, humidity, oMultiGraph);
    moveBubble(name, pm2_5, oMultiGraph);
    moveBubble(name, pm10, oMultiGraph);
    selectGraphHandler(name, speechBubbleMeter);
}

function predictDTHUpdate(name, values, speechBubbleMeter, oMultiGraph) {
    let {pm2_5Predict, pm10Predict} = speechBubbleMeter;
    pm2_5Predict.textContent = values[0].toFixed(1);
    pm10Predict.textContent = values[1].toFixed(1);
    moveBubble(name, pm2_5Predict, oMultiGraph);
    moveBubble(name, pm10Predict, oMultiGraph);
    selectGraphHandler(name, speechBubbleMeter, true);
}

function moveBubble(name, bubble, oMultiGraph) {
    let bubbleWrap = bubble.parentElement.parentElement;
    let key = name + "_" + bubble.parentElement.className;
    let graph = oMultiGraph.graphs[key].graph;
    if (graph.visible) {
        let value = normalize(graph.canvas.height, graph.labels.max.y, graph.labels.min.y, graph.queue[0]);
        bubbleWrap.style.display = "block";
        bubbleWrap.style.transform = "translateY(" + -value + "px)";
    } else bubbleWrap.style.display = "none";
}

/**
 *
 * @param name
 * @param color
 * @param setY
 * @returns {OGraph}
 */
function initOGraph(name, color, setY) {
    let labels = new Labels();
    labels.setX(-16, -0.3, 10, "분");
    labels.setY(...setY);
    let graph = new OGraph(name, color, 2, labels, new Queue(50));
    graph.visibleLabel = false;
    graph.visible = false;
    graph.dynamicNormalize = false;
    return graph;
}

function initOMultiGraph(name) {
    let wrap = document.querySelector("#" + name + " .multiGraphWrap");
    let names = ["temperature", "humidity", "pm2_5", "pm10", "pm2_5Predict", "pm10Predict"];

    let graphs = [];

    names.forEach(n => {
        let graph = undefined;
        switch (n) {
            case "temperature":
                graph = initOGraph(name + "_" + n, "rgba(246, 81, 29, .7)", [-10, 40, 10, "°C"]);
                break;
            case "humidity":
                graph = initOGraph(name + "_" + n, "rgba(255, 180, 0, .7)", [0, 100, 10, "%"]);
                graph.labels.setPosition("y", "right");
                break;
            case "pm2_5Predict":
                graph = initOGraph(name + "_" + n, "rgba(60, 106, 187, .7)", [0, 100, 3, "㎍/㎥"]);
                // graph.visibleLabel = true;
                graph.visible = true;
                break;
            case "pm10Predict":
                graph = initOGraph(name + "_" + n, "rgba(67, 124, 90, .7)", [0, 100, 3, "㎍/㎥"]);
                // graph.visibleLabel = true;
                graph.visible = true;
                break;
            case "pm2_5":
                graph = initOGraph(name + "_" + n, "rgba(0, 166, 237, 1)", [0, 100, 3, "㎍/㎥"]);
                graph.visible = true;
                break;
            case "pm10":
                graph = initOGraph(name + "_" + n, "rgba(127, 184, 0, 1)", [0, 100, 3, "㎍/㎥"]);
                // graph.visibleLabel = true;
                graph.visible = true;
                break;
        }
        if (graph)
            graphs.push(graph);
    });
    return new OMultiGraph(wrap, graphs);
}

/**
 *
 * @param name
 * @returns {{temperature: Element, humidity: Element, pm2_5: Element, pm10: Element}}
 */
function initBubble(name) {
    let wrap = document.querySelector("#" + name);
    return {
        temperature: wrap.querySelector(".bubble .temperature span"),
        humidity: wrap.querySelector(".bubble .humidity span"),
        pm2_5: wrap.querySelector(".bubble .pm2_5 span"),
        pm10: wrap.querySelector(".bubble .pm10 span"),
        pm2_5Predict: wrap.querySelector(".bubble .pm2_5Predict span"),
        pm10Predict: wrap.querySelector(".bubble .pm10Predict span")
    };
}

function selectGraphHandler(name, speechBubbleMeter, predict=false) {
    if (predict) {
        let predictSpan = document.querySelectorAll("#" + name + " .btns .predict > div > span > span");
        predictSpan[0].textContent = speechBubbleMeter.pm2_5Predict.textContent;
        predictSpan[1].textContent = speechBubbleMeter.pm10Predict.textContent;
    } else {
        document.querySelector("#" + name + " .btns .temperature > span > span").textContent = speechBubbleMeter.temperature.textContent;
        document.querySelector("#" + name + " .btns .humidity > span > span").textContent = speechBubbleMeter.humidity.textContent;
        document.querySelector("#" + name + " .btns .pm2_5 > span > span").textContent = speechBubbleMeter.pm2_5.textContent;
        document.querySelector("#" + name + " .btns .pm10 > span > span").textContent = speechBubbleMeter.pm10.textContent;
    }
}

function attachSelectBoxBtnClickHandler(name) {
    let wrap = document.querySelector("#" + name);
    let btns = wrap.querySelectorAll(".selectBox > section > div");
    let predictBtn = wrap.querySelector(".selectBox > section > div.predict");
    let thing = user.things.find(thing => thing.name === name);
    let graphs = thing.oMultiGraph.graphs;
    let mGraphArr = [];
    for (let graphsKey in graphs) {
        if (graphs.hasOwnProperty(graphsKey)) {
            mGraphArr.push(graphs[graphsKey]);
        }
    }
    let measureGraphs = mGraphArr.filter(m => m.graph.name.includes("pm") && !m.graph.name.includes("Predict"));
    let predictGraphs = mGraphArr.filter(m => m.graph.name.includes("Predict"));
    let notPmGraphs = mGraphArr.filter(m => !m.graph.name.includes("Predict"));
    let clickHandler = (sensorType) => {
        let otherGraphs = [];
        if (sensorType === "predict") {
            predictBtn.dataset.visible = predictBtn.dataset.visible !== "true";
        } else {
            let mGraph = graphs[name + "_" + sensorType];
            mGraph.visible = !mGraph.visible;
            if(sensorType.includes("temperature") || sensorType.includes("humidity")) {
                otherGraphs = mGraphArr.filter(m => !m.graph.name.includes("temperature") && !m.graph.name.includes("humidity"));
            } else if(sensorType.includes("pm2_5") || sensorType.includes("pm10")) {
                otherGraphs = mGraphArr.filter(m => !m.graph.name.includes("pm2_5") && !m.graph.name.includes("pm10"));
            }
        }

        hideAll(otherGraphs);

        measureGraphs.forEach(m1 => {
            predictGraphs.forEach(m2 => {
                if (m2.graph.name.includes(m1.graph.name)) {
                    if (predictBtn.dataset.visible === "true") {
                        if (!m2.visible && m1.visible) m2.visible = m1.visible;
                        else if (m2.visible && !m1.visible) m2.visible = m1.visible;
                    } else {
                        if (m2.visible) m2.visible = false;
                    }
                }
            });
        });
        normalizeLabels(thing.oMultiGraph);
        redraw();
    };

    btns.forEach(btn => {
        btn.addEventListener("click", clickHandler.bind(null, btn.dataset.sensorType));
        btn.addEventListener("click", () => {
            btns.forEach(b => {
                notPmGraphs.forEach(m => {
                    if (m.graph.name.includes(b.dataset.sensorType))
                        b.dataset.visible = m.graph.visible;
                });
            });
        });
    });

    function hideAll(graphs) {
        graphs.forEach(graph => {
            if (graph.visible) graph.visible = false;
        });
    }
}

function bubbleRender(thing) {
    let speechBubbleMeterArr = [];
    for (let speechBubbleMeterKey in thing.speechBubbleMeter) {
        if (thing.speechBubbleMeter.hasOwnProperty(speechBubbleMeterKey)) {
            speechBubbleMeterArr.push(thing.speechBubbleMeter[speechBubbleMeterKey]);
        }
    }
    speechBubbleMeterArr.forEach(bubble => {
        moveBubble(thing.name, bubble, thing.oMultiGraph);
    });
}

function redraw() {
    let ja_multiGraphWrap = document.getElementsByClassName("multiGraphWrap");
    let ja_canvases = document.getElementsByTagName("canvas");
    for (let i = 0; i < ja_multiGraphWrap.length; i++) {
        let width = ja_multiGraphWrap[i].parentElement.offsetWidth;
        let height = ja_multiGraphWrap[i].parentElement.offsetHeight;
        ja_multiGraphWrap[i].style.width = width + "px";
        ja_multiGraphWrap[i].style.height = width * .7 + "px";
    }
    for (let j = 0; j < ja_canvases.length; j++) {
        let width = ja_canvases[j].parentElement.parentElement.offsetWidth;
        let height = ja_canvases[j].parentElement.parentElement.offsetHeight;
        ja_canvases[j].width = width;
        ja_canvases[j].height = width * .7;
    }
    user.things.forEach(thing => {
        draw(thing.oMultiGraph);
        bubbleRender(thing);
    });
}

export default function (args) {
    let { oHashController } = args;
    /**
     *
     * @var {{uType: string, provider: string, things: [{_id: string, name: string, __v: number, token: string}], _id: string, email: string, __v: number}} user
     */
    user.thingTokens = [];
    user.things.forEach(thing => {
        user.thingTokens.push(thing.token);
        // thing["drawGraphs"] = initDrawGraph(thing.name);
        thing["oMultiGraph"] = initOMultiGraph(thing.name);
        thing["speechBubbleMeter"] = initBubble(thing.name);
        attachSelectBoxBtnClickHandler(thing.name);
    });
    console.log(user);
    let socket = connectSocket(user);

    socket.on("connect", () => {
        user.things.forEach(thing => {
            attachPowerClickHandler(thing.name, socket);
            //
            // let json = {
            //     name: thing.name,
            //     line: 50
            // };
            // socket.emit("normalizedDTH", json);
        });
    });

    socket.on("power", (data) => {
        if (data.error) console.error(data);
        // socket.disconnect();
    });
    socket.on("realTimeDTH", (result) => {
        let name = result.name;
        let reported = result.data.state.reported;
        let power = reported.power;
        powerStateUpdate(name, power);
        let thing = user.things.find(thing => thing.name === result.name);
        // realTimeDTHUpdate(reported, thing.drawGraphs);
        realTimeDTHUpdate(name, reported, thing.speechBubbleMeter, thing.oMultiGraph);
    });

    socket.on("predictDTH", (result) => {
        if (result.data && result.data.predictions) {
            let name = result.name;
            let thing = user.things.find(thing => thing.name === result.name);
            let values = result.data.predictions[0][result.data.predictions[0].length - 1];
            // canvasHandler(result, thing.drawGraphs);
            canvasHandler(result, thing.oMultiGraph, "predictDTH");
            predictDTHUpdate(name, values, thing.speechBubbleMeter, thing.oMultiGraph);
            // socket.disconnect();
        }
    });

    socket.on("normalizedDTH", (result) => {
        let thing = user.things.find(thing => thing.name === result.name);
        // canvasHandler(result, thing.drawGraphs);
        canvasHandler(result, thing.oMultiGraph, "normalizedDTH");
        // socket.disconnect();
    });


    window.onresize = redraw;
    window.onload = redraw;
    redraw();

}