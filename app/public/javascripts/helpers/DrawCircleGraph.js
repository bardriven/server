/*
 * Copyright (c) 2018. OsirptPm
 * 이 저작물에는 크리에이티브 커먼즈 저작자표시-비영리-동일조건변경허락 4.0 국제 라이선스가 적용 되어 있습니다. 이 라이선스의 설명을 보고 싶으시면 http://creativecommons.org/licenses/by-nc-sa/4.0/ 을(를) 참조하세요.
 */

class DrawCircleGraph {

    set textSecondary (value) {
        this._option.textSecondary = value;
    }
    set textPrimary (value) {
        this._option.textPrimary = value;
    }

    /**
     *
     * @param {{[bgColor]: string, [lineColor]: string, [lineWidth]: number, [textPrimaryColor]: string, [textPrimarySize]: number, [textSecondaryColor]: string, [textSecondarySize]: number, [textPrimary]: string, [textSecondary]: string}} decoration
     */
    set decoration(decoration) {
        this._option.bgColor = decoration.bgColor || this._option.bgColor;
        this._option.lineColor = decoration.lineColor || this._option.lineColor;
        this._option.lineWidth = decoration.lineWidth || this._option.lineWidth;
        this._option.textPrimaryColor = decoration.textPrimaryColor || this._option.textPrimaryColor;
        this._option.textPrimarySize = decoration.textPrimarySize || this._option.textPrimarySize;
        this._option.textSecondaryColor = decoration.textSecondaryColor || this._option.textSecondaryColor;
        this._option.textSecondarySize = decoration.textSecondarySize || this._option.textSecondarySize;
        this._option.textPrimary = decoration.textPrimary || this._option.textPrimary;
        this._option.textSecondary = decoration.textSecondary || this._option.textSecondary;
    }

    /**
     *
     * @param {HTMLCanvasElement} element
     * @param {number} [fixedMaxValue]
     * @param {number} [fixedMinValue]
     */
    constructor(element, fixedMaxValue = 100, fixedMinValue = 0) {
        this.context = element.getContext("2d");

        this._value = 0;

        this._option = {};
        this._option.fixedMaxValue = fixedMaxValue;
        this._option.fixedMinValue = fixedMinValue;

        this._option.bgColor = "rgba(0, 0, 0, .2)";
        this._option.lineColor = "rgba(255, 255, 255, 1)";
        this._option.lineWidth = 20;
        this._option.textPrimaryColor = "rgba(0, 0, 0, 1)";
        this._option.textPrimarySize = 30;
        this._option.textSecondaryColor = "rgba(0, 0, 0, .8)";
        this._option.textSecondarySize = 20;
        this._option.textPrimary = "";
        this._option.textSecondary = "";
    }

    /**
     *
     * @param {number} [val]
     */
    draw(val) {
        this._value = val || this._value;
        let value = this._value;
        this._drawCircle(value);
        this._drawText(value);
    }

    /**
     *
     * @param {number} value
     * @private
     */
    _drawCircle(value) {
        let ctx = this.context;
        let option = this._option;

        let centerX = ctx.canvas.width / 2;
        let centerY = ctx.canvas.height / 2;
        let radius = centerX - 30;

        let percentage = (value < option.fixedMinValue) ? 0 : (value - option.fixedMinValue) / (option.fixedMaxValue - option.fixedMinValue) * 100;
        let startAngle = 1.5 * Math.PI;
        let endAngle = (2 * Math.PI) / 100 * percentage;

        ctx.clearRect(0, 0, ctx.canvas.width, ctx.canvas.height);
        ctx.beginPath();
        ctx.arc(centerX, centerY, radius, 0, 2 * Math.PI, true);
        ctx.lineCap = "round";
        ctx.lineWidth = option.lineWidth;
        ctx.strokeStyle = option.bgColor;
        ctx.stroke();


        ctx.shadowColor = "rgba(0, 0, 0, .4)";
        ctx.shadowBlur = 10;

        ctx.beginPath();
        ctx.arc(centerX, centerY, radius, startAngle, startAngle - endAngle, true);
        ctx.lineCap = "round";
        ctx.lineWidth = option.lineWidth;
        ctx.strokeStyle = option.lineColor;
        ctx.stroke();
    }

    /**
     *
     * @param {number} value
     * @private
     */
    _drawText(value) {
        let ctx = this.context;
        let option = this._option;

        let centerX = ctx.canvas.width / 2;
        let centerY = ctx.canvas.height / 2;

        ctx.font = "bold " + option.textPrimarySize + "px sans-serif";
        ctx.textAlign = "center";
        // ctx.textBaseline = "middle";
        ctx.fillStyle = option.textPrimaryColor;
        ctx.fillText(option.textPrimary, centerX, centerY);
        ctx.font = option.textSecondarySize + "px sans-serif";
        ctx.textAlign = "center";
        // ctx.textBaseline = "middle";
        ctx.fillStyle = option.textSecondaryColor;
        ctx.fillText(option.textSecondary, centerX, centerY + option.textSecondarySize + 5);
    }
}

export default DrawCircleGraph;