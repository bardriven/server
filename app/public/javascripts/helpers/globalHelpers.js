
/*
 * Copyright (c) 2018. OsirptPm
 * 이 저작물에는 크리에이티브 커먼즈 저작자표시-비영리-동일조건변경허락 4.0 국제 라이선스가 적용 되어 있습니다. 이 라이선스의 설명을 보고 싶으시면 http://creativecommons.org/licenses/by-nc-sa/4.0/ 을(를) 참조하세요.
 */

function calcDiscomfortIndex(temperature, humidity) {
    let rh = humidity / 100;
    return 9 / 5 * temperature - 0.55 * (1 - rh) * (9 / 5 * temperature - 26) + 32
}
/**
 *
 * @param {number} value
 * @return {{value: number, step: number, txt: string, title: string}}
 */
function DIConverter(value) {
    let txts = ["낮음", "보통", "높음", "불쾌"];
    let result = {
        value,
        step: 0,
        get txt() {
            return txts[this.step];
        },
        title: "불쾌지수"
    };
    if (value >= 80) result.step = 3;
    if (value >= 75) result.step = 2;
    if (value >= 68) result.step = 1;
    else result.step = 0;
    return result;
}
/**
 *
 * @param {number} value
 * @return {{value: number, step: number, txt: string, title: string}}
 */
function pm2_5Converter(value) {
    let txts = ["좋음", "보통", "나쁨", "매우나쁨"];
    let result = {
        value,
        step: 0,
        get txt() {
            return txts[this.step];
        },
        title: "PM2.5"
    };
    if (value >= 76) result.step = 3;
    else if (value >= 36) result.step = 2;
    else if (value >= 16) result.step = 1;
    else result.step = 0;
    return result;
}
/**
 *
 * @param {number} value
 * @return {{value: number, step: number, txt: string, title: string}}
 */
function pm10Converter(value) {
    let txts = ["좋음", "보통", "나쁨", "매우나쁨"];
    let result = {
        value,
        step: 0,
        get txt() {
            return txts[this.step];
        },
        title: "PM10"
    };
    if (value >= 151) result.step = 3;
    else if (value >= 81) result.step = 2;
    else if (value >= 31) result.step = 1;
    else result.step = 0;
    return result;
}



function get1DArrayFrom2DObj(keyName, arr) {
    let result = [];
    arr.forEach(v => {
        result.push(v[keyName]);
    });
    return result;
}

/**
 *
 * @param shadows
 * @return {{temperatureArr: Array, humidityArr: Array, pm2_5Arr: Array, pm10Arr: Array}}
 */
function getDTHArr(shadows) {
    let stateArr = get1DArrayFrom2DObj("state", shadows);
    let reportedArr = get1DArrayFrom2DObj("reported", stateArr);

    let temperatureArr = get1DArrayFrom2DObj("temperature", reportedArr);
    let humidityArr = get1DArrayFrom2DObj("humidity", reportedArr);
    let pm2_5Arr = get1DArrayFrom2DObj("pm2_5", reportedArr);
    let pm10Arr = get1DArrayFrom2DObj("pm10", reportedArr);
    return {temperatureArr, humidityArr, pm2_5Arr, pm10Arr};
}


// 실제 전체 길이 : 실제 10분의 1 길이 = 가상 전체 길이 : 가상 10분의 1 길이
// 실제 10분의 1 길이 =
/**
 *
 * @param {number} actualTotalValue
 * @param {number} virtualMaxValue
 * @param {number} virtualMinValue
 * @param {number} virtualValue
 * @returns {number} actualValue
 * @private
 */
function normalize(actualTotalValue, virtualMaxValue, virtualMinValue, virtualValue) {
    let virtualTotalValue = virtualMaxValue - virtualMinValue;
    virtualValue -= virtualMinValue;
    let result = (actualTotalValue * virtualValue) / virtualTotalValue;
    return isNaN(result) ? actualTotalValue / 2 : result;
}


function attachPowerClickHandler(name, socket) {
    let powerBtn = $("#"+name).find(".power")[0];
    let powerStatusIcon = $("#"+name).find(".powerStatusIcon")[0];
    powerBtn.onclick = (e) => {
        let json = {
            name,
            power: powerStatusIcon.classList.contains("powerOff")
        };
        socket.emit("power", json);
    }
}



export {calcDiscomfortIndex, DIConverter, pm2_5Converter, pm10Converter, get1DArrayFrom2DObj, getDTHArr, normalize, attachPowerClickHandler};