import {calcDiscomfortIndex, DIConverter, pm2_5Converter, pm10Converter, attachPowerClickHandler} from "../helpers/globalHelpers.js"

function geo_success(position) {
    // let str = `
    //     coords:<br>
    //         accuracy: ${position.coords.accuracy}<br>
    //         altitude: ${position.coords.altitude}<br>
    //         altitudeAccuracy: ${position.coords.altitudeAccuracy}<br>
    //         heading: ${position.coords.heading}<br>
    //         latitude: ${position.coords.latitude}<br>
    //         longitude: ${position.coords.longitude}<br>
    //         speed: ${position.coords.speed}<br>
    //     timestamp: ${position.timestamp}`;
    // $("#outside .row section p")[0].innerHTML = str;

    const getNearbyMsrstnList = coords => {
        let path = "/ajax/getNearbyMsrstnList?utmkX=" + coords.documents[0].x + "&utmkY=" + coords.documents[0].y;
        return requestAjaxGET(path);
    };

    const ArpltnInforInqireSvc = result => {
        // console.log(result);
        let path = "/ajax/ArpltnInforInqireSvc?stationName=" + result[0].stationName;
        let addr = result[0].addr;
        let tm = result[0].tm;
        $("#outside .stationName")[0].innerHTML = addr;
        $("#outside .tm")[0].innerHTML = "측정 위치: 내 위치에서 " + tm + "km";
        return requestAjaxGET(path);
    };

    getUTMK(position.coords.latitude, position.coords.longitude)
        .then(getNearbyMsrstnList)
        .then(ArpltnInforInqireSvc)
        .then(result => {
            // console.log(result);
            let pm2_5 = result[0].pm25Value;
            let pm2_5_24 = result[0].pm25Value24;
            let pm10 = result[0].pm10Value;
            let pm10_24 = result[0].pm10Value24;
            $("#outside .pm2_5")[0].innerHTML = "PM2.5: " + pm2_5 + "㎍/㎥ - " + pm2_5Converter(pm2_5).txt;
            $("#outside .pm10")[0].innerHTML = "PM10: " + pm10 + "㎍/㎥ - " + pm10Converter(pm10).txt;
            // $("#outside .row section p")[0].innerHTML += "pm2.5: " + pm2_5 + "㎍/㎥<br>" +
            //     "pm2.5 예측: " + pm2_5_24 + "㎍/㎥<br>" +
            //     "pm10: " + pm10 + "㎍/㎥<br>" +
            //     "pm10 예측: " + pm10_24 + "㎍/㎥";
        });
}

function geo_error() {
    alert("위치 정보를 사용할 수 없습니다.");
}

let geo_options = {
    enableHighAccuracy: true,
    maximumAge        : 15000,
    timeout           : 10000
};



function getUTMK(latitude, longitude) {
    return new Promise((resolve, reject) => {
        let req = new XMLHttpRequest();
        req.open("GET", "https://dapi.kakao.com/v2/local/geo/transcoord.json?x=" + longitude + "&y=" + latitude + "&input_coord=WGS84&output_coord=UTMK");
        req.setRequestHeader("Authorization", "KakaoAK f40ae130c5f76ed3846f462535a87158");
        req.responseType = "json";
        req.onreadystatechange = aEvt => {
            if (req.readyState === 4) {
                if(req.status === 200)
                    resolve(req.response);
                else
                    reject("Error loading page");
            }
        };
        req.send(null);
    });
}

function requestAjaxGET(path) {
    return new Promise((resolve, reject) => {
        let req = new XMLHttpRequest();
        req.open("GET", path);
        req.responseType = "json";
        req.onreadystatechange = aEvt => {
            if (req.readyState === 4) {
                if(req.status === 200)
                    resolve(req.response);
                else
                    reject("Error loading page");
            }
        };
        req.send(null);
    });
}






export default function () {
    if (navigator.geolocation) {
        // 지오로케이션 사용 가능
        // console.log(navigator.geolocation);
        let wpid = navigator.geolocation.watchPosition(geo_success, geo_error, geo_options);
    } else {
        // 지오로케이션 사용 불가능
        console.log("지오로케이션 사용 불가능");
    }
}