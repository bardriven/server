/*
 * Copyright (c) 2018. OsirptPm
 * 이 저작물에는 크리에이티브 커먼즈 저작자표시-비영리-동일조건변경허락 4.0 국제 라이선스가 적용 되어 있습니다. 이 라이선스의 설명을 보고 싶으시면 http://creativecommons.org/licenses/by-nc-sa/4.0/ 을(를) 참조하세요.
 */

class SimpleRouter {
    /**
     *
     * @return {object}
     */
    get query() {
        return this._query;
    }

    constructor() {
        this._query = {};

        let str = window.location.href.split("?")[1];
        let re = new RegExp("([A-Za-z0-9_\.\-]*)=([A-Za-z0-9_\.\-]*)", "g");
        let result = null;

        while (result = re.exec(str)) {
            this._query[result[1]] = result[2];
        }
    }

    /**
     *
     * @param path
     * @param callback
     */
    route(path, callback) {
        if (window.location.pathname === path) {
            callback();
        }
    }
}

export default SimpleRouter;