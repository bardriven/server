/*
 * Copyright (c) 2018. OsirptPm
 * 이 저작물에는 크리에이티브 커먼즈 저작자표시-비영리-동일조건변경허락 4.0 국제 라이선스가 적용 되어 있습니다. 이 라이선스의 설명을 보고 싶으시면 http://creativecommons.org/licenses/by-nc-sa/4.0/ 을(를) 참조하세요.
 */

export default function connectSocket(user) {
    let socket = io("https://dev.osirptpm.ml:8092/", {
        query: {
            tokens: user.thingTokens,
            from: "client"
        }
    });
    socket.on("connect", () => {
        // console.log("connect");
    });
    socket.on("power", (data) => {
        // console.log(data);
        // socket.disconnect();
    });
    socket.on("realTimeDTH", (data) => {
        // console.log(data);
        // socket.disconnect();
    });
    socket.on("predictDTH", (data) => {
        // console.log(data)
        // console.log(data);
        // socket.disconnect();
    });
    socket.on("normalizedDTH", (data) => {
        // console.log(data);
        // socket.disconnect();
        // let json = {
        //     name: data.name,
        //     data: data.data
        // };
        // socket.emit("predictDTH", json);
    });
    socket.on("error", (reason) => {
        console.log("error", reason);
        socket.disconnect();
    });
    socket.on("disconnect", (reason) => {
        console.log("disconnect", reason);
    });
    user.things.forEach(thing => {
        // setInterval(() => {
        //     let json = {
        //         name: thing.name,
        //         line: 50
        //     };
        //     socket.emit("normalizedDTH", json);
        // }, 20000);
    });
    return socket;
}