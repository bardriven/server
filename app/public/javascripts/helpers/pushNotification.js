// https://github.com/GoogleChromeLabs/web-push-codelab/blob/master/app/scripts/main.js
function urlB64ToUint8Array(base64String) {
    const padding = "=".repeat((4 - base64String.length % 4) % 4);
    const base64 = (base64String + padding)
        .replace(/\-/g, "+")
        .replace(/_/g, "/");

    const rawData = window.atob(base64);
    const outputArray = new Uint8Array(rawData.length);

    for (let i = 0; i < rawData.length; ++i) {
        outputArray[i] = rawData.charCodeAt(i);
    }
    return outputArray;
}

function pushNotification() {
    const applicationServerPublicKey = "BFxdj0WrOvu3RXn_LTgIx0G7dqNRsC1wEvcMbHUuCFcbPuQEO1ZVYSCA3D1QwO9f0lRI8LXUDN9eawGs5-A2GMA"; //UWVzb7Qx2KlQEHVCKBVYiCyCs0B4ybxTKTsuLEGFDZU
    let swRegistration, isSubscribed;
    let pushButton = document.getElementById("subscribe");

    if ("serviceWorker" in navigator && "PushManager" in window) {
        console.log("Service Worker and Push is supported");

        navigator.serviceWorker.register("/javascripts/helpers/pushNotification_worker/sw.js")
            .then(swReg => {
                console.log("Service Worker is registered", swReg);

                swRegistration = swReg;
                initialiseUI();
            })
            .catch(error => {
                console.error("Service Worker Error", error);
            });
    } else {
        console.warn("Push messaging is not supported");
        pushButton.textContent = "Push Not Supported";
    }

    function initialiseUI() {
        pushButton.addEventListener("click", function() {
            pushButton.disabled = true;
            if (isSubscribed) {
                unsubscribeUser();
            } else {
                subscribeUser();
            }
        });

        // Set the initial subscription value
        swRegistration.pushManager.getSubscription()
            .then(subscription => {
                isSubscribed = !(subscription === null);

                if (isSubscribed) {
                    console.log("User IS subscribed.");
                } else {
                    console.log("User is NOT subscribed.");
                }

                updateBtn();
            });
    }

    function updateBtn() {
        if (Notification.permission === "denied") {
            pushButton.textContent = "Push Messaging Blocked.";
            pushButton.disabled = true;
            updateSubscriptionOnServer(null);
            return;
        }

        if (isSubscribed) {
            pushButton.textContent = "Disable Push Messaging";
        } else {
            pushButton.textContent = "Enable Push Messaging";
        }

        pushButton.disabled = false;
    }

    function subscribeUser() {
        const applicationServerKey = urlB64ToUint8Array(applicationServerPublicKey);
        swRegistration.pushManager.subscribe({
            userVisibleOnly: true,
            applicationServerKey: applicationServerKey
        })
            .then(subscription => {
                console.log("User is subscribed:", subscription);

                updateSubscriptionOnServer(subscription);

                isSubscribed = true;

                updateBtn();
            })
            .catch(err => {
                console.log("Failed to subscribe the user: ", err);
                updateBtn();
            });
    }

    function updateSubscriptionOnServer(subscription) {
        console.log(subscription);
        console.log(JSON.stringify(subscription));
    }

    function unsubscribeUser() {
        swRegistration.pushManager.getSubscription()
            .then(function(subscription) {
                if (subscription) {
                    return subscription.unsubscribe();
                }
            })
            .catch(function(error) {
                console.log("Error unsubscribing", error);
            })
            .then(function() {
                updateSubscriptionOnServer(null);

                console.log("User is unsubscribed.");
                isSubscribed = false;

                updateBtn();
            });
    }
}

export default pushNotification;