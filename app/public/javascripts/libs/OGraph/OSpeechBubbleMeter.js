/*
 * Copyright (c) 2018. OsirptPm
 * 이 저작물에는 크리에이티브 커먼즈 저작자표시-비영리-동일조건변경허락 4.0 국제 라이선스가 적용 되어 있습니다. 이 라이선스의 설명을 보고 싶으시면 http://creativecommons.org/licenses/by-nc-sa/4.0/ 을(를) 참조하세요.
 */

//
// class ORectangle {
//     set onChange(value) {
//         this._onChange = value;
//     }
//     get value() {
//         return this._value;
//     }
//
//     set value(value) {
//         this._value = this._normalize(this._context.canvas.height, this.max, this.min, value);
//         this._onChange();
//     }
//     // /**
//     //  *
//     //  * @returns {string}
//     //  */
//     // get name() {
//     //     return this._name;
//     // }
//     // /**
//     //  *
//     //  * @param {string} value
//     //  */
//     // set name(value) {
//     //     this._name = value;
//     // }
//     // /**
//     //  *
//     //  *
//     //  * @returns {number}
//     //  */
//     // get h() {
//     //     return this._h;
//     // }
//     // /**
//     //  *
//     //  * @param {number} value
//     //  */
//     // set h(value) {
//     //     this._h = value;
//     // }
//     // /**
//     //  *
//     //  *
//     //  * @returns {number}
//     //  */
//     // get w() {
//     //     return this._w;
//     // }
//     // /**
//     //  *
//     //  * @param {number} value
//     //  */
//     // set w(value) {
//     //     this._w = value;
//     // }
//     // /**
//     //  *
//     //  *
//     //  * @returns {number}
//     //  */
//     // get x() {
//     //     return this._x;
//     // }
//     // /**
//     //  *
//     //  * @param {number} value
//     //  */
//     // set x(value) {
//     //     this._x = value;
//     // }
//     // /**
//     //  *
//     //  *
//     //  * @returns {number}
//     //  */
//     // get y() {
//     //     return this._y;
//     // }
//     // /**
//     //  *
//     //  * @param {number} value
//     //  */
//     // set y(value) {
//     //     this._y = value;
//     // }
//
//     constructor(context, name, height, min, max) {
//         this._context = context;
//         this._name = name;
//         this._h = height;
//         this.max = max;
//         this.min = min;
//         this._value = 0;
//         this._onChange = () => {};
//     }
//
//     draw() {
//         let pad = 10;
//         let lineWidth = this._context.lineWidth;
//         this._context.strokeRect(pad, this._context.canvas.height - this._value, this._context.canvas.width - pad - lineWidth- lineWidth, this._h);
//     }
//
//     // 실제 전체 길이 : 실제 10분의 1 길이 = 가상 전체 길이 : 가상 10분의 1 길이
//     // 실제 10분의 1 길이 =
//     /**
//      *
//      * @param {number} actualTotalValue
//      * @param {number} virtualMaxValue
//      * @param {number} virtualMinValue
//      * @param {number} virtualValue
//      * @returns {number} actualValue
//      * @private
//      */
//     _normalize(actualTotalValue, virtualMaxValue, virtualMinValue, virtualValue) {
//         let virtualTotalValue = virtualMaxValue - virtualMinValue;
//         virtualValue -= virtualMinValue;
//         let result = (actualTotalValue * virtualValue) / virtualTotalValue;
//         return isNaN(result) ? actualTotalValue / 2 : result;
//     }
// }
//
//
// class OSpeechBubbleMeter {
//     get canvas() {
//         return this._canvas;
//     }
//     /**
//      *
//      * @returns {Array<ORectangle>}
//      */
//     get rects() {
//         return this._rects;
//     }
//
//     /**
//      *
//      * @param {HTMLCanvasElement} canvas
//      */
//     constructor(canvas) {
//         this._canvas = canvas;
//         this.context = canvas.getContext("2d");
//         /**
//          *
//          * @type {Array<ORectangle>}
//          */
//         this._rects = [];
//     }
//
//     /**
//      *
//      * @param {string} name
//      * @param {number} height
//      * @param {number} min
//      * @param {number} max
//      * @param {number} [value=0]
//      */
//     addRect(name, height, min, max, value=0) {
//         let rect = new ORectangle(this.context, name, height, min, max);
//         rect.onChange = () => {
//             this.draw();
//         };
//         this._rects.push(rect);
//         rect.value = value;
//         return rect;
//     }
//
//     /**
//      *
//      * @param {ORectangle} rect
//      */
//     removeRect(rect) {
//         this._rects.remove(rect);
//         this.draw();
//     }
//
//     draw() {
//         let ctx = this.context;
//         ctx.clearRect(0, 0, this._canvas.width, this._canvas.height);
//         this._rects.forEach(rect => {
//             rect.draw();
//         });
//     }
// }
//
//
// let div = document.createElement("div");
// let canvas = document.createElement("canvas");
// div.style.width = "200px";
// div.style.height = "700px";
// canvas.width = 200;
// canvas.height = 700;
// div.style.backgroundColor = "#222";
// document.body.appendChild(div);
// let oSBM = new OSpeechBubbleMeter(canvas);
// div.appendChild(oSBM.canvas);
// let rect = oSBM.addRect("테스트", 50, 0, 100, 50);
// console.log(oSBM);

function createBubble(name) {
    let bubble = document.createElement("section");
    bubble.className = "bubble "  + name;
    bubble.style.width = "100%";
    bubble.style.height = "20%";

}