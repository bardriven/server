/*
 * Copyright (c) 2018. OsirptPm
 * 이 저작물에는 크리에이티브 커먼즈 저작자표시-비영리-동일조건변경허락 4.0 국제 라이선스가 적용 되어 있습니다. 이 라이선스의 설명을 보고 싶으시면 http://creativecommons.org/licenses/by-nc-sa/4.0/ 을(를) 참조하세요.
 */

class Queue extends Array {
    /**
     *
     * @param {function} func
     */
    set onAdd(func) {
        this._onAdd = func;
    }

    /**
     *
     * @param {number} capacity
     * @param [initialValue]
     */
    constructor(capacity, initialValue) {
        super();
        this._onAdd = () => {};
        this.capacity = capacity;
        this.length = capacity;
        this.fill(initialValue !== undefined ? initialValue : 0);
    }

    /**
     *
     * @param {* | Array<*>} item
     * @returns {* | Array<*>} item
     */
    add(item) {
        let result = undefined;
        if (item.constructor.name === "Array") {
            this.unshift(...item.reverse());
            if (this.length > this.capacity) {
                result = this.splice(this.capacity, this.length);
            }
        }
        else {
            this.unshift(item);
            if (this.length > this.capacity) {
                result = this.remove();
            }
        }
        this._onAdd();
        return result;
    }

    /**
     *
     * @returns {* | undefined}
     */
    remove() {
        return this.pop();
    }

}

class Labels {
    /**
     *
     * @returns {{x: string, y: string}}
     */
    get position() {
        return this._position;
    }
    /**
     *
     * @returns {Array<number>}
     */
    get x() {
        return this._x;
    }
    /**
     *
     * @returns {Array<number>}
     */
    get y() {
        return this._y;
    }
    /**
     *
     * @returns {{x: number, y: number}}
     */
    get max() {
        return { x: this.maxX, y: this.maxY }
    }
    /**
     *
     * @returns {{x: number, y: number}}
     */
    get min() {
        return { x: this.minX, y: this.minY }
    }

    constructor() {
        this._x = [];
        this._y = [];
        this._X = "x";
        this._Y = "y";
        this.xSuffix = "";
        this.ySuffix = "";
        this._position = {x: "bottom", y: "left"};
    }

    /**
     *
     * @param {string} label "x|y"
     * @param {string} pos "x: top|bottom, y: left|right"
     */
    setPosition(label, pos) {
        this.position[label] = pos;
    }

    /**
     *
     * @returns {Array<string>}
     */
    getX() {
        return this._x.map(x => x + this.xSuffix);
    }
    /**
     *
     * @returns {Array<string>}
     */
    getY() {
        return this._y.map(y => y + this.ySuffix);
    }

    /**
     *
     * @param {number} min
     * @param {number} max
     * @param {number} column
     * @param {string} [suffix]
     */
    setX(min, max, column, suffix) {
        if (suffix !== undefined) this.xSuffix = suffix;
        this.maxX = max;
        this.minX = min;
        this._set(min, max, column, this._X);
    }

    /**
     *
     * @param {number} min
     * @param {number} max
     * @param {number} column
     * @param {string} [suffix]
     */
    setY(min, max, column, suffix) {
        if (suffix !== undefined) this.ySuffix = suffix;
        this.maxY = max;
        this.minY = min;
        this._set(min, max, column, this._Y);
    }

    _set(min, max, column, labelName) {
        let shortcut = (max - min) / (column - 1);
        let arr = [];
        for (let i = min; arr.length < column; i+=shortcut) {
            arr.push(i > max ? parseFloat(max.toFixed(1)) : parseFloat(i.toFixed(1)));
        }
        if (labelName === this._X) this._x = arr;
        else this._y = arr;
    }
}

class OGraph {
    /**
     *
     * @returns {Labels}
     */
    get labels() {
        return this._labels;
    }
    /**
     *
     * @param {Labels} value
     */
    set labels(value) {
        this._labels = value;
    }
    /**
     *
     * @returns {boolean}
     */
    get visibleLabel() {
        return this._visibleLabel;
    }
    /**
     *
     * @param {boolean} value
     */
    set visibleLabel(value) {
        this._visibleLabel = value;
    }
    /**
     *
     * @returns {boolean}
     */
    get visible() {
        return this._visible;
    }
    /**
     *
     * @param {boolean} value
     */
    set visible(value) {
        this._visible = value;
    }
    /**
     *
     * @returns {string}
     */
    get name() {
        return this._name;
    }
    /**
     *
     * @param {string} value
     */
    set name(value) {
        this._name = value;
    }
    /**
     *
     * @returns {HTMLCanvasElement}
     */
    get canvas() {
        return this._canvas;
    }
    /**
     *
     * @returns {Queue}
     */
    get queue() {
        return this._queue;
    }
    /**
     *
     * @param {Queue} value
     */
    set queue(value) {
        this._queue = value;
        this._queue.onAdd = () => {
            // this._setLabels();
            this.draw();
        };
    }

    /**
     *
     * @returns {boolean}
     */
    get dynamicNormalize() {
        return this._dynamicNormalize;
    }
    /**
     *
     * @param {boolean} value
     */
    set dynamicNormalize(value) {
        this._dynamicNormalize = value;
    }

    /**
     *
     * @param {string} name
     * @param {string} color
     * @param {number} lineWidth
     * @param {Labels} labels
     * @param {Queue} queue
     */
    constructor(name, color, lineWidth, labels, queue) {
        this._name = name;
        this.strokeStyle = color;
        this.lineWidth = lineWidth;
        this._labels = labels;
        this._dynamicNormalize = true;
        this.queue = queue;
        this._canvas = document.createElement("canvas");
        // this._canvas.width = 1000;
        // this._canvas.height = 700;
        this.context = this.canvas.getContext("2d");

        this._visible = true;
        this._visibleLabel = true;
    }

    draw() {
        if (this.visible) {this._setLabels(); this._draw();}
        if (this.visibleLabel) this._drawLabels();
    }

    /**
     *
     * @private
     */
    _draw() {
        let ctx = this.context;
        let width = ctx.canvas.width;
        let height = ctx.canvas.height;

        let queue = this._queue;

        // let halfPaddedW = option.w - option.padding;
        // let nonPaddedW = halfPaddedW - option.padding;
        // let halfPaddedH = option.h - option.padding;
        // let nonPaddedH = halfPaddedH - option.padding;
        // let column = queue.length - 1;
        // let shortcut = nonPaddedW / column;
        //
        // let decoration = option.decoration;
        let column = queue.length - 1;
        let shortcut = width / column;

        ctx.clearRect(0, 0, width, height);
        ctx.beginPath();

        ctx.strokeStyle = this.strokeStyle;
        ctx.lineWidth = this.lineWidth;
        ctx.lineJoin = "round";

        let pad = this.lineWidth * 2;
        let yPad = height / 90;
        let yPad2 =  yPad * 2;

        ctx.moveTo(width + pad, height - yPad);
        ctx.lineTo(width + pad, height - this._normalize(height - yPad2, this._labels.max.y, this._labels.min.y, queue[0]) - yPad);
        ctx.lineTo(width, height - this._normalize(height - yPad2, this._labels.max.y, this._labels.min.y, queue[0]) - yPad);
        for (let i = 1; i < queue.length; i++) {
            ctx.lineTo((column - i) * shortcut, height - this._normalize(height - yPad2, this._labels.max.y, this._labels.min.y, queue[i]) - yPad);


        }
        ctx.lineTo(-pad, height - this._normalize(height - yPad2, this._labels.max.y, this._labels.min.y, queue[queue.length - 1]) - yPad);
        ctx.lineTo(-pad, height - yPad);
        // ctx.closePath();

        let fillStyle = this.strokeStyle.split(",");
        fillStyle[3] = ".2)";
        let sFillStyle = fillStyle.join(",");
        fillStyle[3] = ".0)";
        let eFillStyle = fillStyle.join(",");

        let grd = ctx.createLinearGradient(0, height - this._normalize(height, this._labels.max.y, this._labels.min.y, Math.max(...queue)), 0, height);
        grd.addColorStop(0, sFillStyle);
        grd.addColorStop(1, eFillStyle);
        ctx.fillStyle = grd;
        ctx.fill();

        ctx.stroke();
    }

    /**
     *
     * @private
     */
    _drawLabels() {
        this._drawLabelX();
        this._drawLabelY();
    }
    /**
     *
     * @private
     */
    _drawLabelX() {
        let ctx = this.context;
        let values = this._labels.getX();
        let pos = this._labels.position.x;
        let width = ctx.canvas.width;
        let height = ctx.canvas.height;
        let pad = height / 20;

        let column = values.length - 1;
        let shortcut = width / column;

        ctx.fillStyle = "white";
        ctx.font="13px sans-serif";

        // ctx.textAlign = "left";
        // ctx.fillText(values[0], pad, height - pad);
        ctx.textAlign = "center";
        if (pos === "top") {
            for (let i = 1; i < values.length - 1; i++) {
                ctx.fillText(values[i], i * shortcut, pad + 11);
            }
            ctx.textAlign = "right";
            // ctx.fillText(values[values.length - 1], (values.length - 1) * shortcut - pad, pad + 11);
        } else {
            if (values.length <= 2) {
                ctx.textAlign = "left";
                ctx.fillText(values[0], pad, height - pad);
            }
            for (let i = 1; i < values.length - 1; i++) {
                ctx.fillText(values[i], i * shortcut, height - pad);
            }
            ctx.textAlign = "right";
            ctx.fillText(values[values.length - 1], (values.length - 1) * shortcut - pad, height - pad);
        }
    }
    /**
     *
     * @private
     */
    _drawLabelY() {
        let ctx = this.context;
        let values = this._labels.getY();
        let pos = this._labels.position.y;
        let width = ctx.canvas.width;
        let height = ctx.canvas.height;
        let padY = height / 20;

        let column = values.length - 1;
        let shortcut = height / column;

        ctx.fillStyle = "white";
        ctx.font="13px sans-serif";

        let charLength = values.map(x => x.length);
        let max = Math.max(...charLength);
        let pad = width / 40;
        ctx.textAlign = "right";
        // ctx.fillText(values[0], pad, height - padY);
        if (pos === "left") {
            pad = max * 13 * .85;
            if (values.length <= 2) {
                ctx.fillText(values[0], pad, height - padY - 16);
            }
            for (let i = 1; i < values.length - 1; i++) {
                ctx.fillText(values[i], pad, height - i * shortcut);
            }
            ctx.fillText(values[values.length - 1], pad, padY + 11);
        } else {
            for (let i = 1; i < values.length - 1; i++) {
                ctx.fillText(values[i], width - pad, height - i * shortcut);
            }
            // ctx.fillText(values[values.length - 1], width - pad, padY + 11);
        }

    }

    // 실제 전체 길이 : 실제 10분의 1 길이 = 가상 전체 길이 : 가상 10분의 1 길이
    // 실제 10분의 1 길이 =
    /**
     *
     * @param {number} actualTotalValue
     * @param {number} virtualMaxValue
     * @param {number} virtualMinValue
     * @param {number} virtualValue
     * @returns {number} actualValue
     * @private
     */
    _normalize(actualTotalValue, virtualMaxValue, virtualMinValue, virtualValue) {
        let virtualTotalValue = virtualMaxValue - virtualMinValue;
        virtualValue -= virtualMinValue;
        let result = (actualTotalValue * virtualValue) / virtualTotalValue;
        return isNaN(result) ? actualTotalValue / 2 : result;
    }

    // 라벨 재설정
    _setLabels() {
        let columnY = this.canvas.height / 70 - 1;
        let columnX = this.canvas.width / 200;
        columnY = columnY < 2 ? 2 : columnY;
        columnX = columnX < 2 ? 2 : columnX;
        if(this._labels) {
            if (this.dynamicNormalize) {
                this._labels.setY(Math.min(...this._queue) - .1, Math.max(...this._queue) + .1, columnY);
                this._labels.setX(this._labels.min.x, this._labels.max.x, columnX);
            }
        }
    }
}


class OMultiGraph {
    /**
     *
     * @param {Element} element
     * @param {Array<OGraph>} graphs
     */
    constructor(element, graphs) {
        this.element = element;
        this._setElementStyle();
        /**
         *
         * @type {{graph: OGraph, visible: boolean}}
         */
        this.graphs = {};
        this.graphNames = [];
        let self = this;
        graphs.forEach(graph => {
            graph.canvas.style.position = "absolute";
            graph.canvas.style.top = "0px";
            graph.canvas.style.left = "0px";
            // graph.canvas.width = this.element.width;
            // graph.canvas.height = this.element.height;
            this.element.appendChild(graph.canvas);
            this.graphNames.push(graph.name);
            this.graphs[graph.name] = {
                graph,
                set visible(value) {
                    graph.visible = value;
                    self.showNHide(graph.canvas, value);
                    if (value) graph.draw();
                },
                get visible() {
                    return graph.visible;
                }
            };
        });
    }

    _setElementStyle() {
        this.element.style.position = "relative";
    }

    /**
     *
     * @param {Array<string>} names
     */
    show(names) {
        names.forEach(name => {
            this.graphs[name].visible = true;
        });
    }
    /**
     *
     * @param {Array<string>} names
     */
    hide(names) {
        names.forEach(name => {
            this.graphs[name].visible = false;
        });
    }

    showNHide(canvas, bool) {
        if (bool) this.element.appendChild(canvas);
        else this.element.removeChild(canvas);
    }

    /**
     *
     * @param {Labels} labels
     */
    integratedLabel(labels) {
        this.graphNames.forEach((name, i) => {
            if (this.graphs[name].graph.visible) {
                this.graphs[name].graph.visibleLabel = this.graphNames.length - 1 === i;
                this.graphs[name].graph.dynamicNormalize = false;
                this.graphs[name].graph.labels = labels;
                this.graphs[name].graph.draw();
            }
        });
    }

}

export {Queue, Labels, OGraph, OMultiGraph};
//
// let queue1 = new Queue(50, 0);
// queue1.add([1, 2, 4, 5, 7, 9, -6, -5, -3, 5, 6, 8, 12, 34, 56, 23, 12, 7, 4, 22, 55, 77, 54, 32, 21, 11, -6, -4, -5, 6, 8, 12, 34, 56, 23, 12, 7, 4, 12, 34, 56, 23, 12, 32, 21, 11, 6, 9, 6]);
// let queue2 = new Queue(50, 0);
// queue2.add([1, 2, 4, 5, 7, 8, 12, 34, 56, 23, 12, 32, 21, 11, 6, 9, 5, -7, -9, -6, 5, 3, 5, 6, 8, 12, 34, 56, 23, 12, 7, 5, 7, 9, 6, -5, -3, 5, 6, 8, 12, 34, 34, 23, 12, 7, 6, 5, 3, 5, 6, 4, 7, 4, 22, 55, 34, 54]);
// let labels1 = new Labels();
// labels1.setY(-10, 100, 10);
// labels1.setX(16, 0, 10, "분 전");
// let labels2 = new Labels();
// labels2.setY(-10, 999, 10);
// labels2.setX(-16, 0, 10, "분 전");
// labels2.setPosition("x", "top");
// labels2.setPosition("y", "right");
// let graph1 = new OGraph("test1", "rgba(34, 200, 234, .5)", 2, labels1, queue1);
// let graph2 = new OGraph("test2", "rgba(234, 200, 34, .5)", 2, labels2, queue2);
// let div = document.createElement("div");
// div.style.width = "1000px";
// div.style.height = "700px";
// div.style.backgroundColor = "#222";
// document.body.appendChild(div);
// let multiGraph = new OMultiGraph(div, [graph1, graph2]);
// multiGraph.integratedLabel(labels1);
// graph1.draw();
// queue1.add(22);
// queue2.add(12);
// console.log(multiGraph);