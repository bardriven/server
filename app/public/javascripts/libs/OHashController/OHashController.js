/*
 * Copyright (c) 2018. OsirptPm
 * 이 저작물에는 크리에이티브 커먼즈 저작자표시-비영리-동일조건변경허락 4.0 국제 라이선스가 적용 되어 있습니다. 이 라이선스의 설명을 보고 싶으시면 http://creativecommons.org/licenses/by-nc-sa/4.0/ 을(를) 참조하세요.
 */

class HashValueNFunc {
    get value() {
        return this._value;
    }
    set value(string) {
        if (string.constructor.name !== "String") throw TypeError("Not a String");
        this._value = string;
    }
    get on() {
        return this._on;
    }
    set on(callback) {
        if (callback.constructor.name !== "Function") throw TypeError("Not a Function");
        this._on = callback;
    }
    get off() {
        return this._off;
    }
    set off(callback) {
        if (callback.constructor.name !== "Function") throw TypeError("Not a Function");
        this._off = callback;
    }
}

class OHashController {
    constructor() {
        this._hashValueNFuncs = {};
        window.addEventListener("hashchange", this.runRegisteredFunc.bind(this));
    }

    // 비공개 속성
    /**
     *
     * @param {Object} object
     * @private
     */
    set _hashValueNFuncs(object) {
        if (object.constructor.name !== "Object") throw TypeError("Not a Object");
        this.__hashValueNFuncs = object;
    }

    /**
     *
     * @return {Set<string>}
     * @private
     */
    get _hashSet() {
        return new Set(location.hash.substring(1).split("#"));
    }

    // 공개 속성
    /**
     *
     * @return {Object}
     */
    get hashValueNFuncs() {
        return this.__hashValueNFuncs;
    }

    // 사용자 호출 메소드

    // 해시 컨트롤 관련

    add(hashValue) {
        let hashSet = this._hashSet;
        hashSet.add(hashValue);
        this._setHashString(hashSet);
    }
    remove(hashValue) {
        let hashSet = this._hashSet;
        hashSet.delete(hashValue);
        this._setHashString(hashSet);
    }
    toggle(hashValue) {
        let hashSet = this._hashSet;
        if (hashSet.has(hashValue)) hashSet.delete(hashValue);
        else hashSet.add(hashValue);
        this._setHashString(hashSet);
    }
    replace(oldHashValue, newHashValue) {
        let hashSet = this._hashSet;
        hashSet.delete(oldHashValue);
        hashSet.add(newHashValue);
        this._setHashString(hashSet);
    }

    // 이벤트 관련

    // 이벤트 등록
    /**
     *
     * @param {string | HashValueNFunc} hashValue
     * @param {Function} onFunc
     * @param {Function} offFunc
     * @return {OHashController}
     */
    addHashEvent(hashValue, onFunc, offFunc) {
        let keyNFunc;
        if (hashValue.constructor.name === "HashValueNFunc") {
            keyNFunc = hashValue;
        } else {
            keyNFunc = new HashValueNFunc();
            keyNFunc.value = hashValue;
            keyNFunc.on = (onFunc.constructor.name === "Function") ? onFunc : () => {};  // 해당 함수
            keyNFunc.off = (offFunc.constructor.name === "Function") ? offFunc : () => {};  // 해당 함수
        }
        this.hashValueNFuncs[keyNFunc.value] = keyNFunc;

        // 해시가 이미 있는 경우에 이벤트를 늦게 추가해서 실행 안되는 걸 방지
        this.toggle(hashValue);
        this.toggle(hashValue);
        return this; // 메소드 체인을 위해 this 리턴
    }
    // 이벤트 제거
    /**
     *
     * @param {string} hashValue
     */
    removeHashEvent(hashValue) {
        if (hashValue.constructor.name !== "String") throw TypeError("Not a String");
        delete this.hashValueNFuncs[hashValue];
    }

    // 해당 해시값의 등록 함수 실행
    runRegisteredFunc(event) {
        let oldURL = event.oldURL;
        let newURL = event.newURL;

        let oldHashSet = new Set(oldURL.split("#").slice(1));
        let newHashSet = new Set(newURL.split("#").slice(1));

        let hashValueNFuncs = this.hashValueNFuncs;
        for (let key in hashValueNFuncs) {
            if (newHashSet.has(key)){
                if (!oldHashSet.has(key)) hashValueNFuncs[key].on();
            } else {
                if (oldHashSet.has(key)) hashValueNFuncs[key].off();
            }
        }
    }

    // 비공개 메소드

    /**
     *
     * @param {Set} set
     * @return {string}
     * @private
     */
    _setHashString(set) {
        // console.log(set);
        if (set.constructor.name !== "Set") throw TypeError("Not a Set");
        let str = "";
        set.forEach(v => str += (v !== "") ? "#" + v : "");

        let bodyScrollTop = document.documentElement.scrollTop || document.body.scrollTop; // 스크롤탑으로 가는거 방지

        location.hash = str;

        document.body.scrollTop = bodyScrollTop; // 스크롤탑으로 가는거 방지
        document.documentElement.scrollTop = bodyScrollTop; // 스크롤탑으로 가는거 방지
    }
}

export {OHashController, HashValueNFunc};