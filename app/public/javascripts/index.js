/*
 * Copyright (c) 2018. OsirptPm
 * 이 저작물에는 크리에이티브 커먼즈 저작자표시-비영리-동일조건변경허락 4.0 국제 라이선스가 적용 되어 있습니다. 이 라이선스의 설명을 보고 싶으시면 http://creativecommons.org/licenses/by-nc-sa/4.0/ 을(를) 참조하세요.
 */

import { OHashController } from "./libs/OHashController/OHashController.js";
import TokenStorage from "./helpers/TokenStorage.js";
import SimpleRouter from "./helpers/SimpleRouter.js";
import home from "./home.js";
import control_board from "./control_board.js";
import dashboard from "./dashboard.js";

let oHashController = new OHashController();
let router = new SimpleRouter();
let tokenStorage = new TokenStorage();

let userToken = "usertoken";

if (tokenStorage.getToken(userToken) && !router.query[userToken]) {
    window.location.search = userToken + "=" + tokenStorage.getToken(userToken);
} else if (router.query[userToken]) {
    tokenStorage.setToken(userToken, router.query[userToken]);
} else if (window.location.pathname !== "/") {
    // redirect("/");
}

// 로그인 하면 제어판으로 이동
router.route("/", () => {
    if (tokenStorage.getToken(userToken)) {
        redirect("/dashboard");
        return;
    }
    home({oHashController});
});

router.route("/control_board", () => {
    control_board({oHashController});
});

router.route("/dashboard", () => {
    dashboard({oHashController});
});

router.route("/user/signout", () => {
    tokenStorage.clear();
    redirect("/");
});

//
// $("#signout").on("click", () => {
//     tokenStorage.clear();
// });

function redirect(href) {
    if (tokenStorage.getToken(userToken)) window.location.href = href + "?" + userToken + "=" + tokenStorage.getToken(userToken);
    else window.location.href = href;
}