/*
 * Copyright (c) 2018. OsirptPm
 * 이 저작물에는 크리에이티브 커먼즈 저작자표시-비영리-동일조건변경허락 4.0 국제 라이선스가 적용 되어 있습니다. 이 라이선스의 설명을 보고 싶으시면 http://creativecommons.org/licenses/by-nc-sa/4.0/ 을(를) 참조하세요.
 */

const { requestPromise } = require("../helpers/CustomRequest");
const { requestPromiseExternalAPI } = require("../helpers/CustomRequest");
const { StatusError } = require("../helpers/CustomError");
const errorHandler = require("../helpers/errorHandler");
const config = require("../config");
const fs = require("fs");

let apiOptions = config.api;

module.exports.home = (req, res, next) => {
    res.render("home", {
        title: "home",
    });
};

module.exports.info = (req, res, next) => {
    let license = fs.readFileSync("../app/public/etc/라이센스.txt").toString().split("\r\n\r\n");
    license = license.map(lic => lic.split("\r\n"));
    res.render("info", {
        title: "info",
        license: license
    });
};

// require usertoken
// input: none
// output: user with thingsTokens
// 사물 토큰을 넣어서 유저 정보 리턴
module.exports.control_board = (req, res, next) => {
    let user = req.user;

    res.render("control_board", {
        title: "Control Panel",
        user
    });
};

module.exports.dashboard = (req, res, next) => {
    let user = req.user;

    res.render("dashboard", {
        title: "dashboard",
        user
    });
};

module.exports.settings = (req, res, next) => {
    let user = req.user;

    res.render("settings", {
        title: "settings",
        user
    });
};

module.exports.getNearbyMsrstnList = (req, res, next) => {
    let { utmkX, utmkY } = req.query;
    let serviceKey = "Q%2Bthg%2BSyuSt7esHeW0EpPIceYj2tHS766MBO6kACnZG249ZqMwyjbKOOfxP6h0%2FcRq0EdIVTrBZk8b5bWDE2bQ%3D%3D";
    let path = "http://openapi.airkorea.or.kr/openapi/services/rest/MsrstnInfoInqireSvc/getNearbyMsrstnList?ver=1.0&_returnType=json&tmX=" + utmkX + "&tmY=" + utmkY + "&pageNo=1&numOfRows=10&ServiceKey=" + serviceKey;


    requestPromiseExternalAPI(null, path, "GET")
        .then(result => {
            res.json(result.list);
        }).catch(next);
};

module.exports.ArpltnInforInqireSvc = (req, res, next) => {
    let { stationName } = req.query;
    stationName = encodeURIComponent(stationName);
    let serviceKey = "Q%2Bthg%2BSyuSt7esHeW0EpPIceYj2tHS766MBO6kACnZG249ZqMwyjbKOOfxP6h0%2FcRq0EdIVTrBZk8b5bWDE2bQ%3D%3D";
    let path = "http://openapi.airkorea.or.kr/openapi/services/rest/ArpltnInforInqireSvc/getMsrstnAcctoRltmMesureDnsty?stationName=" + stationName + "&ver=1.3&_returnType=json&dataTerm=month&pageNo=1&numOfRows=10&ServiceKey=" + serviceKey;


    requestPromiseExternalAPI(null, path, "GET")
        .then(result => {
            res.json(result.list);
        }).catch(next);
};
