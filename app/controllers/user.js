/*
 * Copyright (c) 2018. OsirptPm
 * 이 저작물에는 크리에이티브 커먼즈 저작자표시-비영리-동일조건변경허락 4.0 국제 라이선스가 적용 되어 있습니다. 이 라이선스의 설명을 보고 싶으시면 http://creativecommons.org/licenses/by-nc-sa/4.0/ 을(를) 참조하세요.
 */

const { requestPromise } = require("../helpers/CustomRequest");
const { StatusError } = require("../helpers/CustomError");
const errorHandler = require("../helpers/errorHandler");
const config = require("../config");

let apiOptions = config.api;

/*
token = request(email, password, db-server)
res.json(token)
 */
module.exports.signin = (req, res, next) => {
    let path = "/users/auth/token" + "?apikey=" + apiOptions.APIKey;
    let { email, password } = req.body;
    let postData = {
        email: email,
        password: password,
        provider: "local"
    };

    const respond = (result) => {
        if (result.success) res.redirect("/?usertoken=" + result.data);
    };
    const onError = (error) => {
        if(error.message === "존재하지 않는 이메일") {
            res.redirect("/?err=invalid_email");
        } else {
            next(error);
        }
    };

    requestPromise(postData, apiOptions.api_server + path)
        .then(respond)
        .catch(onError);
};

module.exports.signup = (req, res, next) => {
    let path = "/users" + "?apikey=" + apiOptions.APIKey;
    let { email, password, password_check, thing_name, thing_key } = req.body;

    if (password !== password_check) return next(new StatusError("비밀번호 재확인 필요"), 412);

    let postData = {
        email,
        password,
        password_check,
        thing_name,
        thing_key,
        provider: "local"
    };

    const respond = (result) => {
        res.redirect("/");
    };

    requestPromise(postData, apiOptions.api_server + path)
        .then(respond)
        .catch(next);
};

module.exports.password = (req, res, next) => {
    let user = req.user;
    let checkPath = "/users/auth/token" + "?apikey=" + apiOptions.APIKey;
    let { password, new_password, new_password_check } = req.body;
    if (new_password !== new_password_check) return next(new StatusError("비밀번호 재확인 필요"), 412);

    let checkPostData = {
        email: user.email,
        password: password,
        provider: "local"
    };

    const respondToken = (result) => {
        if (result.success) {
            return Promise.resolve();
        } else return Promise.reject(result.error);
    };

    const requestP = () => {
        let path = "/users/" + user._id + "?apikey=" + apiOptions.APIKey;
        let postData = {
            password: new_password,
            password_check: new_password_check
        };
        return requestPromise(postData, apiOptions.api_server + path, "PUT");
    };

    const respond = (result) => {
        res.redirect("/settings");
    };

    requestPromise(checkPostData, apiOptions.api_server + checkPath)
        .then(respondToken)
        .then(requestP)
        .then(respond)
        .catch(next);
};

module.exports.withdraw = (req, res, next) => {
    let user = req.user;
    let checkPath = "/users/auth/token" + "?apikey=" + apiOptions.APIKey;
    let { password } = req.body;
    if (!password) return next(new StatusError("비밀번호 확인 필요"), 412);

    let checkPostData = {
        email: user.email,
        password: password,
        provider: "local"
    };

    const respondToken = (result) => {
        if (result.success) {
            return Promise.resolve();
        } else return Promise.reject(result.error);
    };

    const requestP = () => {
        let path = "/users/" + user._id + "?apikey=" + apiOptions.APIKey;
        return requestPromise({}, apiOptions.api_server + path, "DELETE");
    };

    const respond = (result) => {
        res.redirect("/user/signout");
    };

    requestPromise(checkPostData, apiOptions.api_server + checkPath)
        .then(respondToken)
        .then(requestP)
        .then(respond)
        .catch(next);
};


module.exports.signout = (req, res, next) => {
    res.render("signout", {
        title: "signout"
    });
};

module.exports.thingAdd = (req, res, next) => {
    let user = req.user;
    let { thing_name, thing_key } = req.body;

    if (!thing_name || !thing_key) return next(new StatusError("입력 값 확인", 412));

    let path = "/users/" + user._id + "/things?apikey=" + apiOptions.APIKey;
    let postData = {
        thing_name, thing_key
    };

    const respond = (result) => {
        res.redirect("/settings?usertoken=" + user.token);
    };

    requestPromise(postData, apiOptions.api_server + path, "PUT")
        .then(respond)
        .catch(next);
};

module.exports.thingRemove = (req, res, next) => {
    let user = req.user;
    let { thing_id } = req.body;

    if (!thing_id) return next(new StatusError("입력 값 확인", 412));

    let path = "/users/" + user._id + "/things/" + thing_id + "?apikey=" + apiOptions.APIKey;

    const respond = (result) => {
        res.redirect("/settings?usertoken=" + user.token);
    };

    requestPromise({}, apiOptions.api_server + path, "DELETE")
        .then(respond)
        .catch(next);
};