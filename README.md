# Web Server

스마트 공기청정기 이용 사용자가 사용할 웹의 서버.


주된 기능


유저관리, 기기관리, 기기 상태 모니터링 및 제어, 주변 미세먼지 농도 확인.


인증은 토큰방식 이용, 기기 데이터 전달은 Websocket 사용.

모바일 실행 영상
https://youtu.be/_ZMRa_gvZwY

데스크탑 실행 영상
https://youtu.be/OJ9rOYIRD4E

전체 프로젝트 주소 https://bitbucket.org/account/user/bardriven/projects/AW