MitalApp
Copyright (c) 2018. OsirptPm
이 저작물에는 크리에이티브 커먼즈 저작자표시-비영리-동일조건변경허락 4.0 국제 라이선스가 적용 되어 있습니다. 이 라이선스의 설명을 보고 싶으시면 http://creativecommons.org/licenses/by-nc-sa/4.0/ 을(를) 참조하세요.

cookie-parser
https://github.com/expressjs/cookie-parser
Copyright (c) 2014 TJ Holowaychuk <tj@vision-media.ca>
Copyright (c) 2015 Douglas Christopher Wilson <doug@somethingdoug.com>
MIT License

debug
https://github.com/visionmedia/debug
Copyright (c) 2014 TJ Holowaychuk <tj@vision-media.ca>
MIT License

express
https://github.com/expressjs/express
Copyright (c) 2009-2014 TJ Holowaychuk <tj@vision-media.ca>
Copyright (c) 2013-2014 Roman Shtylman <shtylman+expressjs@gmail.com>
Copyright (c) 2014-2015 Douglas Christopher Wilson <doug@somethingdoug.com>
MIT License

http-errors
https://github.com/jshttp/http-errors
Copyright (c) 2014 Jonathan Ong me@jongleberry.com
Copyright (c) 2016 Douglas Christopher Wilson doug@somethingdoug.com
MIT License

morgan
https://github.com/expressjs/morgan
Copyright (c) 2014 Jonathan Ong <me@jongleberry.com>
Copyright (c) 2014-2017 Douglas Christopher Wilson <doug@somethingdoug.com>
MIT License

pug
https://github.com/mrmckain/PUG
Copyright (c) 2015 Michael R. McKain
MIT License

request
https://github.com/request/request
Copyright (c) 2004 Mikeal 
Apache License

aws-iot-device-sdk
https://github.com/aws/aws-iot-device-sdk-js
Copyright (c) 2004 fengsongAWS
Apache License

aws-sdk
https://github.com/aws/aws-sdk-java
Copyright (c) 2004 AWS
Apache License

jsonwebtoken
https://github.com/auth0/node-jsonwebtoken
Copyright (c) 2015 Auth0, Inc. <support@auth0.com> (http://auth0.com)
MIT License
 
socket.io
https://github.com/socketio/socket.io
Copyright (c) 2014-2018 Automattic <dev@cloudup.com>
MIT License

socket.io-client
https://github.com/socketio/socket.io-client
Copyright (c) 2014 Guillermo Rauch
MIT License

bcrypt
https://github.com/kelektiv/node.bcrypt.js
Copyright (c) 2010 Nicholas Campbell
MIT License

mongoose
https://github.com/Automattic/mongoose
Copyright (c) 2004-2013 Sergey Lyubka <valenok@gmail.com>
Copyright (c) 2013-2018 Cesanta Software Limited
GPLv2 License

serialport
https://github.com/node-serialport/node-serialport
Copyright 2010 Christopher Williams. All rights reserved.
MIT License

jquery
https://github.com/jquery/jquery
Copyright JS Foundation and other contributors, https://js.foundation/
MIT License

bootstrap
https://github.com/angular-ui/bootstrap
Copyright (c) 2012-2017 the AngularUI Team, https://github.com/organizations/angular-ui/teams/291112
MIT License

Materiial Icons
https://material.io/tools/icons/?style=baseline
Apache License

Do hyeon
https://fonts.google.com/specimen/Do+Hyeon?selection.family=Do+Hyeon
Copyright (c) Woowahan Brothers
with Reserved Font Name Do hyeon.
SIL OPEN FONT LICENSE

Nanum Brush Script
https://fonts.google.com/specimen/Nanum+Brush+Script
Copyright (c) Sandoll
with Reserved Font Name Nanum Brush Script
SIL OPEN FONT LICENSE

Nanum Myeongjo
https://fonts.google.com/specimen/Nanum+Myeongjo
Copyright (c) Fontrix
Copyright (c) Sandoll
with Reserved Font Name Nanum Myeongjo
SIL OPEN FONT LICENSE

tensorflow-gpu
https://github.com/timsainb/Tensorflow-MultiGPU-VAE-GAN
Copyright (c) 2016 Tim Sainburg
MIT License

numpy
https://github.com/numpy/numpy
Copyright (c) 2005-2017, NumPy Developers.
2-clause BSD LICENSE

boto3
https://github.com/boto/boto3
Copyright (c) 2013-2017 Amazon.com, Inc.
Apache License

matplotlib
https://github.com/matplotlib/matplotlib
Copyright (c) Matplotlib Development Team("MDT")
Copyright (c) 1997-2011 by Secret Labs AB
Copyright (c) 1995-2011 by Fredrik Lundh
Python Software Foundation (PSF) license

DHT22
https://github.com/gosouth/DHT22-cpp
Copyright (c) 2011 by Ben Adams
Apache License